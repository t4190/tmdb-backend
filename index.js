require("dotenv").config();
const express = require("express");
const moviesRoutes = require("./routes/movies");

const app = express();
const port = 8000;

app.use("/movies", moviesRoutes);

app.listen(port, () => {
  console.log("TMDB app listening at http://localhost:8000");
});
