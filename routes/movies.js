const express = require("express");
const axios = require("axios");
const router = express.Router();

router.get("/search", (req, res) => {
  const params = {
    api_key: process.env.TMDB_API_KEY,
    query: req.query.query,
    page: req.query.page,
  };
  axios
    .get("https://api.themoviedb.org/3/search/movie", { params })
    .then(({ data }) => {
      res.send(data);
    })
    .catch((err) => console.log(err));
});

router.get("/search/:id", (req, res) => {
  const params = {
    api_key: process.env.TMDB_API_KEY,
  };
  axios
    .get(`https://api.themoviedb.org/3/movie/${req.params.id}`, { params })
    .then(({ data }) => {
      res.send(data);
    })
    .catch((err) => console.log(err));
});

router.get("/similar/:id", (req, res) => {
  const params = {
    api_key: process.env.TMDB_API_KEY,
  };

  axios
    .get(`https://api.themoviedb.org/3/movie/${req.params.id}/similar`, {
      params,
    })
    .then(({ data }) => res.send(data))
    .catch((err) => console.log(err));
});

module.exports = router;
